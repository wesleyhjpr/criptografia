﻿using Xunit;

namespace Hash.Test
{
    public class HashTest
    {
        [Fact(DisplayName = "O_hash_inalterado_corresponde_ao_texto1")]
        public void O_hash_inalterado_corresponde_ao_texto1()
        {
            var senha = "passw0rd";
            var sal = Salt.Criar();
            var hash = Hash.Criar(senha, sal);
            
            var combinacao = Hash.Validar(senha, sal, hash);
            
            Assert.True(combinacao);
        }

        [Fact(DisplayName = "Hash_violado_nao_corresponde_ao_texto1")]
        public void Hash_violado_nao_corresponde_ao_texto1()
        {            
            var senha = "passw0rd";
            var sal = Salt.Criar();
            var hash = "blahblahblah";
            
            var combinacao = Hash.Validar(senha, sal, hash);
            
            Assert.False(combinacao);
        }
        [Fact(DisplayName = "Hash_de_duas_mensagens_diferentes_nao_coincidem1")]
        public void Hash_de_duas_mensagens_diferentes_nao_coincidem1()
        {            
            var senha1 = "passw0rd";
            var senha2 = "password";
            var sal = Salt.Criar();
            
            var hash1 = Hash.Criar(senha1, sal);
            var hash2 = Hash.Criar(senha2, sal);
            
            Assert.True(hash1 != hash2);
        }
        [Fact(DisplayName = "Hash_de_duas_mensagens_iguais_nao_coincidem1")]
        public void Hash_de_duas_mensagens_iguais_nao_coincidem1()
        {
            var senha1 = "passw0rd";
            var sal1 = Salt.Criar();

            var senha2 = "passw0rd";
            var sal2 = Salt.Criar();

            var hash1 = Hash.Criar(senha1, sal1);
            var hash2 = Hash.Criar(senha2, sal2);

            Assert.True(hash1 != hash2);
        }

        [Fact(DisplayName = "O_hash_inalterado_corresponde_ao_texto2")]
        public void O_hash_inalterado_corresponde_ao_texto2()
        {            
            var senha = "passw0rd";
            var hash = Hash.CriarSenhaHash(senha);
            
            var combinacao = Hash.VerificarSenhaHash(senha, hash);
            
            Assert.True(combinacao);
        }
        [Fact(DisplayName = "Hash_violado_nao_corresponde_ao_texto2")]
        public void Hash_violado_nao_corresponde_ao_texto2()
        {            
            var senha = "passw0rd";
            var hash = "blahblahblahblah";
            
            var combinacao = Hash.VerificarSenhaHash(senha, hash);
            
            Assert.False(combinacao);
        }
        [Fact(DisplayName = "Hash_de_duas_mensagens_diferentes_nao_coincidem2")]
        public void Hash_de_duas_mensagens_diferentes_nao_coincidem2()
        {            
            var senha1 = "passw0rd";
            var senha2 = "password";
            
            var hash1 = Hash.CriarSenhaHash(senha1);
            var hash2 = Hash.CriarSenhaHash(senha2);
            
            Assert.True(hash1 != hash2);
        }
        [Fact(DisplayName = "Hash_de_duas_mensagens_iguais_nao_coincidem2")]
        public void Hash_de_duas_mensagens_iguais_nao_coincidem2()
        {
            var senha1 = "passw0rd";
            var senha2 = "passw0rd";

            var hash1 = Hash.CriarSenhaHash(senha1);
            var hash2 = Hash.CriarSenhaHash(senha2);

            Assert.True(hash1 != hash2);
        }
    }
}
