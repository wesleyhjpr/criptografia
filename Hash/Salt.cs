﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Hash
{
    public class Salt
    {
        public static string Criar()
        {
            byte[] randomBytes = new byte[128 / 8];
            using (var gerador = RandomNumberGenerator.Create())
            {
                gerador.GetBytes(randomBytes);
                return Convert.ToBase64String(randomBytes);
            }
        }
    }
}
