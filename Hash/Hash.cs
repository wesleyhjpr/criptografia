﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Linq;
using System.Text;

namespace Hash
{
    public class Hash
    {
        static readonly KeyDerivationPrf algoritmo = KeyDerivationPrf.HMACSHA256;
        public static string Criar(string value, string salt)
        {
            var valueBytes = KeyDerivation.Pbkdf2(
                                password: value,
                                salt: Encoding.UTF8.GetBytes(salt),
                                prf: algoritmo,
                                iterationCount: 10000,
                                numBytesRequested: 256 / 8);

            return Convert.ToBase64String(valueBytes);
        }
        public static bool Validar(string senha, string sal, string hash)
            => Criar(senha, sal) == hash;

        public static string CriarSenhaHash(string senha)
        {
            var sal = Convert.FromBase64String(Salt.Criar());
            const int qtdInteracao = 10000;
            const int tamanhoDoSal = 128 / 8;

            var subChave = KeyDerivation.Pbkdf2(
                                password: senha,
                                salt: sal,
                                prf: algoritmo,
                                iterationCount: qtdInteracao,
                                numBytesRequested: 256 / 8);

            var saidaBytes = new byte[13 + sal.Length + subChave.Length];
            saidaBytes[0] = 0x01; // marcador de formato
            GravarOrdemDeBytesDaRede(saidaBytes, 1, (uint)algoritmo);
            GravarOrdemDeBytesDaRede(saidaBytes, 5, qtdInteracao);
            GravarOrdemDeBytesDaRede(saidaBytes, 9, tamanhoDoSal);
            Buffer.BlockCopy(sal, 0, saidaBytes, 13, sal.Length);
            Buffer.BlockCopy(subChave, 0, saidaBytes, 13 + tamanhoDoSal, subChave.Length);

            return Convert.ToBase64String(saidaBytes);

        }
        public static bool VerificarSenhaHash(string providedPassword, string hashedPassword)
        {
            var hashSenhaDecodificada = Convert.FromBase64String(hashedPassword);

            // Versão errada
            if (hashSenhaDecodificada[0] != 0x01)
                return false;

            // Ler informação do cabeçalho
            var prf = (KeyDerivationPrf)LerOrdemDeBytesDaRede(hashSenhaDecodificada, 1);
            var qtdInteracao = (int)LerOrdemDeBytesDaRede(hashSenhaDecodificada, 5);
            var tamanhoDoSal = (int)LerOrdemDeBytesDaRede(hashSenhaDecodificada, 9);

            // ler o sal: deverá ser >= 128 bits
            if (tamanhoDoSal < 128 / 8)
            {
                return false;
            }
            var sal = new byte[tamanhoDoSal];
            Buffer.BlockCopy(hashSenhaDecodificada, 13, sal, 0, sal.Length);

            // ler a subChave (o resto do payload): deverá ser >= 128 bits
            var subChaveTamanho = hashSenhaDecodificada.Length - 13 - sal.Length;
            if (subChaveTamanho < 128 / 8)
            {
                return false;
            }
            var subChaveEseperada = new byte[subChaveTamanho];

            Buffer.BlockCopy(hashSenhaDecodificada, 13 + sal.Length, subChaveEseperada, 0, subChaveEseperada.Length);

            // Hash da senha recebida e verifique-a
            var atualSubChave = KeyDerivation.Pbkdf2(providedPassword, sal, prf, qtdInteracao, subChaveTamanho);

            return atualSubChave.SequenceEqual(subChaveEseperada);
        }
        private static void GravarOrdemDeBytesDaRede(byte[] buffer, int offset, uint value)
        {
            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)(value >> 0);
        }
        private static uint LerOrdemDeBytesDaRede(byte[] buffer, int offset)
        {
            return ((uint)(buffer[offset + 0]) << 24)
                | ((uint)(buffer[offset + 1]) << 16)
                | ((uint)(buffer[offset + 2]) << 8)
                | ((uint)(buffer[offset + 3]));
        }
    }
}
